<?php
/*

Memory dump extractor
---------------------

Matthias Viranyi <code@viranyi.com>
2015-03-25

*/
date_default_timezone_set("Europe/Berlin");

function get_line_starting_with ($content, $needle) {
    $needleLength = strlen($needle);
    $lines = explode("\n", $content);
    foreach ($lines as $line) {
        $cleanLine = trim($line);
        if (substr($cleanLine,0,$needleLength) == $needle) {
            return $cleanLine;
        }
    }
    return false;
}

function get_alloc_unit ($content) {
    $curr = get_line_starting_with( $content, "Applications Memory Usage" );
    $currency = substr($curr,count($curr)-5,count($curr)-3);
    return $currency;
}

function get_total_line ($content) {
    return get_line_starting_with( $content, "TOTAL" );
}

function get_pss_total ($line) {
	$lineParts = explode(" ", trim(substr($line,5)));
	return $lineParts[0];
}

//

$recordFilename   = "memdump_record_".date("U").".tsv";
$fileRecordHandle = fopen($recordFilename,"w+");
$identifier       = $argv[1];

//

echo "\nMemdump watching ".$identifier."\n\n";

//

fwrite($fileRecordHandle,"Timestamp\tAllocation\n");
for ($i = 1; true/*$i < 10*/; $i++) {
	exec("adb shell dumpsys meminfo ".$identifier." > memdump.tmp");
	$memdump  = file_get_contents("memdump.tmp");
	$pssTotal = get_pss_total(get_total_line($memdump));
    $currency = get_alloc_unit($memdump);
	fwrite($fileRecordHandle,date("U")."\t".$pssTotal."\n");
	echo "Pss Total: ".$pssTotal." ".$currency."\n";
	sleep(1);
}
fclose( $fileRecordHandle );

echo "Bye bye.";
exit(0);
?>