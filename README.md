# PHP Android Development Toolkit

CLI helper scripts on top of Android's Development Kit written in PHP.

## Overview

* ```adt-memory``` deals with memory related things


## adt-memory

Currently outputting and recording total PSS memory usage on Android devices.

Use:

```
php adt-memory.php com.company.appidentifier
```

Output to Shell:

```
Memdump watching com.company.appidentifier

Pss Total: 42219 kB
Pss Total: 42536 kB
...
```

Output to file:

In addition you get a textual file ```memdump_record_{TIMESTAMP}.tsv``` which you can pass to gnuplot or any other application that understands tab seperated values.

```
Timestamp	Allocation
1429528025	42219
1429528027	42536
``` 